//Declare dependencies and model
const Member = require("../models/members");
const express = require("express");
const router = express.Router(); //to handle routing

//-------MEMBERS
// 1) CREATE MEMBER 
router.post("/", async (req, res) =>{
	const member = new Member(req.body);
	// member.save().then(() => {res.send(member)})
	// .catch((e) => {res.status(400).send(e)})
	try{
		await member.save();
		res.send(member);
	}catch(e){
		res.status(400).send(e)
	}
})
//2 GET ALL MMEMBER

router.get("/", async (req, res)=>{
	// Member.find().then((members) => { return res.status(200).send(members)})
	// .catch((e) => { return res.status(500).send(e)})
	try {
		const member = await Member.find();
		res.status(200).send(member)
	} catch(e){
		return res.status(404).send(e)
	}
}) 
//3)GET ONE MEMBER
router.get("/:id", async (req, res) =>{
	const _id = req.params.id;
	// Member.findById(_id).then((member) => {if(!member){
	// 	return res.status(404).send(e)
	// } return res.send(member)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const member = await Member.findById(_id);
		if(!member){
		return res.status(404).send("WALANG MAHANAP NA member")};
		res.send(member)
	}catch(e){
		res.status(500).send(e)
	}
})
//4)UPDATE ONE MEMBER
router.patch("/:id", async (req, res) =>{
	const _id = req.params.id
	// Member.findByIdAndUpdate(_id, req.body, {new:true}).then((member) => {
	// 	if(!member){return res.status(404).send(e)}
	// 	return res.send(member)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const member = await Member.findByIdAndUpdate(_id, req.body, { new:true });
		if(!member){
		return res.status(404).send("WALANG MAHANAP")};
		res.send(member)
	} catch(e){
		return res.status(500).send(e)
		}
})
//5)DELETE ONE MEMBER
router.delete("/:id", async (req, res)=> {
	const _id = req.params.id;
	// Member.findByIdAndDelete(_id).then((member) => {if(!member){return res.status(404).send(e)}
	// 	return res.send(member)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const member = await Member.findByIdAndDelete(_id);
		if(!member){
			return res.status(404).send("Member doesn't exist or whatever")
		}	res.send(member)
	}catch(e){
		res.status(500).send(e.message)
	}
})

module.exports = router;